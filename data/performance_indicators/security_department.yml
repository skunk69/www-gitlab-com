- name: Security Hiring Actual vs Plan
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Security".
  target: 
  org: Security Department
  health:
    level: 3
    reasons:
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8636794
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"
- name: Security Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Security Department
  is_key: false
  health:
    level: 0
    reasons:
    - Currently finance tells me when there is a problem, I’m not self-service.
    - Get the budget captured in a system
    - Chart budget vs. actual over time in periscope
  urls:
  - https://app.periscopedata.com/app/gitlab/633239/Security-Non-Headcount-BvAs
- name: Security Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
    Data comes from BambooHR and is the average location factor of all team members
    in the Security department.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security Operations having challenges pulling quality candidates in geo-diverse
      locations, in order to hit expectations and allow the team to grow, we will
      revisit our backlog US-based candidates.
  sisense_data:
    chart: 7864119
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"
- name: Security Handbook Update Frequency
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/security` over
    time.
  target: 100
  org: Security Department
  is_key: false
  health:
    level: 0
    reasons:
    - Unknown.
  sisense_data:
    chart: 8073973
    dashboard: 621064
    shared_dashboard: feac7198-86db-480b-9eae-c41cb479a209
    embed: v2
- name: Security Department Narrow MR Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: Security Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is a key indicator showing how productive our team members are based
    on the average MR merged per team member. We currently count all members of the
    Security Department (Director, EMs, ICs) in the denominator because this is a
    team effort. The <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv">projects
    included</a> contributes to the overall product development efforts.
  target: Greater than TBD MRs per Month
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4446
  sisense_data:
    chart: 8934521
    dashboard: 686930
    shared_dashboard: ec910110-91bd-4a08-84aa-223bf6b3c772
    embed: v2
- name: MTTM (Mean-Time-To-Mitigation) for severity::1-severity::2-severity::3 security vulnerabilities
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The MTTM metric is an indicator of our efficiency in mitigating security
    vulnerabilities, whether they are reported through HackerOne bug bounty program
    (or other external means, such as security@gitlab.com emails) or internally-reported.
    The average days to close issues in the GitLab CE project (project_id = '278964')
    that are have the label `security` and severity::1, severity::2, or severity::3; this excludes issues with
    variation of the security label (e.g. `security review`) and severity::4 issues. Issues
    that are not yet closed are excluded from this analysis. This means historical
    data can change as older issues are closed and are introduced to the analysis.
    The average age in days threshold is set at the daily level.
  target: 
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - MTTM metrics continue to show the effectiveness of severity::1/severity::2/severity::3 labelling and follow-up
      in GitLab project
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/WIP:-Appsec-hackerone-vulnerability-metrics?widget=8729826&udv=0
- name: Average age of currently open bug bounty vulnerabilities by severity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The average age of currently open bug bounty vulnerabilities gives a
    health snapshot of how fast we are fixing the vulnerabilities that currently exist.
    This is important because it can help indicate what our future MTTM will look
    like and whether we are meeting our defined SLAs. The query is built by using
    the `ultimate_parent_id` of `9970` and is only for `open` `state` issues labelled
    with `security` and `hackerone`. The average age is measured in days, and the
    targets for each severity are defined in https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues.
  target: https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Average age of currently open bug bounty vulnerabilities by severity gives a
      present health snapshot of our ability to fix vulnerabilites.
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/WIP:-Appsec-hackerone-vulnerability-metrics?widget=8715519&udv=0
- name: Net vulnerability count for each month
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The net vulnerability count for each month allows us to see whether
    we are adding or eliminating from the security backlog. This is the open count
    - close count for each month. It's important to note that these are only for bug
    bounty reported vulnerabilites. This chart is intended to be used with other data
    to try and illustrate a more detailed story.
  target: 0 or less
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - The security vulnerability backlog has only been slightly increasing over the
      past few months.
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/Appsec-hackerone-vulnerability-metrics?widget=8421209&udv=0
- name: Volume of accounts mitigated for abusive activity over time
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The Trust & Safety team is responsible for tracking, automating, and
    reporting on blocked abusive accounts on GitLab.com, and tracking reduction in
    cloud spend where the activities are due to abuse. This metric is the volume of
    accounts mitigated for abusive activity. We are in the process of developing a
    KPI around the efficacy of our abuse report handling.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 0
    reasons:
    - Trust & Safety is working with Defend to ensure that existing and upcoming tooling
      ends up being part of the product with no duplication of efforts among the teams.
  urls:
  - https://app.periscopedata.com/app/gitlab/642878/Abuse-Mitigation-Dashboard
- name: HackerOne budget vs actual (with forecast of 'unknowns')
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We currently run a public bug bounty program through HackerOne, and
    this program has been largely successful - we get a lot of hacker engagement,
    and since the program went public, we have been able to resolve nearly 100 reported
    security vulnerabilities. The bounty spend is however, a budgeting forecast concern
    because of the unpredictability factor from month to month.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - H1 spend has been decreasing, even after the bump to "critical" and "high" findings.
      Projected spend for Q1 20 is $80k, compared to Q1 19's $180k.
- name: Security Risk Heatmap
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The purpose of the Security Operational Risk Management (“STORM”) program
    at GitLab is to identify, track, and treat security operational risks. Ultimately,
    the management of operational risks helps support the broader organization wide
    objectives by ensuring that security risks that may impact GitLab's ability to
    achieve its customer commitments and operational objectives are effectively identified
    and treated in order to sustain and maintain the operational stability of GitLab's
    offerings.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - The 2020 Annual Security Risk Assessment has been completed and the [executive
      summary](https://docs.google.com/document/d/1HTZ6Zh8YZLQ9Yu80zuqkqcA8Rk0SJokbur9ezUnO6g4/edit?usp=sharing)
      is available for internal review.
    - Since completion of the ARA, 4 moderate ad hoc risks have been confirmed and
      10 ad hoc risks are currently in triage.
  urls:
  - https://docs.google.com/document/d/17KILvZyIgk6kDjjlwpjOrZFFReqdCtnSHbcsbMFcvMU/edit#
- name: SIRT (former Security Operations) Page Volume
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the volume and severity of paged incidents
    to the Security Incident Response Team.
  target: 
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - Volume of pages for critical security incidents in Q2 2020 (to date) has increased
      in accordance to projections based on data from the previous 3 quarters
    - Due to unforeseen circumstances affecting our hiring capability, the team is
      operating with a low, sub-optimal number of people on the on-call roster, which
      is impacting project-based work
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9217413&udv=0
- name: Security Control Health
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: GCF security controls are conitnuously tested as parts of the Compliance
    team's continuous monitoring program, internal audits and external audits. A clear
    indicator of success is directly reflected in the control effectveness rating.
    Observations are a result of GCF security failure, indicating that the control
    is not implemented, designed effectively or is not operating effectively. These
    observations indicate a gap that requires remediation in order for the security
    control to be operating and audit ready.
  target: 
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - There are unremediated high-risk observations that are impacting an upcoming
      audit. Remediating high-risk observations is the current priority of the security
      compliance team.
  urls:
  - https://docs.google.com/spreadsheets/d/157htqF6TB_6vjt47zP9n2GN3lPGVPRVPPcnLu5ayPYk/edit?usp=sharing
- name: Security Impact on IACV
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The Field Security organization functions as a sales and customer enablement
    team therefore a clear indicator of success is directly reflected in the engagement
    of their services by Legal, Sales, TAMs and customers themselves.
  target: 
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - Field Security completed Sales security enablement training for new Sales staff
      on July 17th specifically focused on when and how to engage.
    - There is a significant amount of reliance being placed the SOC 2 Type 1 report
      produced by the Security Compliance orgranization by GitLab customers. This
      is a strong enablement tool.
  urls:
  - https://docs.google.com/spreadsheets/d/1nGlg_8PYRpEsr1bkrnTUijy6s3JDaA_Fls6YC9bssM4/edit?usp=sharing
- name: Cost of Fraud
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the Financial impact of abusive accounts.
  target: 
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - TBD. Trust & Safety metric, Ops owns completing this within 90 days. As an interim
      measure we could report on number of fraudulent accounts month over month
  urls:
  - 
- name: HackerOne outreach and engagement
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The true mark of a successful public bug bounty program is partially
    tied to hacker outreach and engagement. We’ve already implemented quite a bit
    of security automation to engage hackers. For example, when findings are submitted
    by hackers, an automated dynamic message is generated by our automation scripts
    and they receive a response with expected ETA on a reply (this ETA is dependent
    on how many findings are in our ‘to be triaged’ bucket at that time, so the ETA
    changes). This ensures the hacker knows 1) we received the report, 2) a human
    will be reading the report within a certain ETA, and 3) a human will be replying
    within an ETA. Our program went public in mid-December 2018, and we are now focusing
    on outreach and incentive programs to retain the top hackers to our program -
    about 90%+ of the most impactful reports are currently submitted by our Top 5-10
    hackers, so it is imperative we retain those individuals.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are getting applications for roles in the Security Department from long time
      H1 researchers.
    - We marked the $1 million bug bounty mark by publishing a blog post that received
      acclaim within the global security community.
- name: Security Response Times
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: Although the Security Department is always working towards minimizing
    friction to day-to-day company operations, it is never possible to be 100% frictionless
    when adding an additional step into an existing process. As cumbersome as this
    may feel (despite our best efforts), this is usually a necessary step to maturing
    our security posture and meeting customer requirements (especially large enterprise
    customers). We plan to measure Security response times to new processes such as
    access request approval issues and security reviews for 3rd party vendor requests
    to highlight where bottlenecks are to the current process, and use a data-driven
    approach to iterate on better processes.
  target: 
  org: Security Department
  is_key: false
  health:
    level: 0
    reasons:
    - Note that this is a newly proposed KPI, and is not ready for today's meeting,
      but we will plan to show metrics around response times for future metrics/KPIs
      meeting.
- name: Security Discretionary Bonus Rate
  base_path: "/handbook/engineering/security/performance-indicators/index.html#security-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Security department
    every month.
  target: 10%
  org: Security Department
  is_key: false
  health:
    level: 0
    reasons:
    - We currently track bonus percentages in aggregate, but there is no easy way
      to see the percentage for each individual department.
- name: Security Department New Hire Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.66
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've been fluxuating above and below the target recently, which is to be expected
      given the size of the department
  sisense_data:
    chart: 9389232
    dashboard: 719541
    embed: v2
