- name: Engineering Hiring Actual vs Plan
  base_path: "/handbook/engineering/performance-indicators/"
  parent: "/handbook/hiring/performance_indicators/#hires-vs-plan"
  definition: Employees are in the division `Engineering`.
  target: 525 engineering members by Jan 31, 2019
  org: Engineering Function
  health:
    level: 3
    reasons:
    - Engineering is on plan
  sisense_data:
    chart: 8497647
    dashboard: 463858
    embed: v2
- name: Engineering Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/performance-indicators/"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Engineering Function
  is_key: true
  health:
    level: 3
    reasons:
    - We are below our budget significantly
  urls:
  - https://app.periscopedata.com/app/gitlab/633290/Engineering-Non-Headcount-BvAs
- name: Engineering Average Location Factor
  base_path: "/handbook/engineering/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: ".58 average location factor"
  org: Engineering Function
  is_key: false
  health:
    level: 2
    reasons:
    - We aren't recruiting much, so we can't influence this positively
    - It is above target, and there is a slow, steady updward trend
  sisense_data:
    chart: 6272001
    dashboard: 463858
    embed: v2
- name: Diversity
  base_path: "/handbook/engineering/performance-indicators/"
  definition: Diversity, Inclusion & Belonging  is one of our core values, and a general
    challenge for the tech industry. GitLab is in a privileged position to positively
    impact diversity in tech because our remote lifestyle should be more friendly
    to people who may have left the tech industry, or studied a technical field but
    never entered industry. This means we can add to the diversity of our industry,
    and not just play a zero-sum recruiting game with our competitors.
  target: It's against company policy to set diversity quotas (but we may add benchmarks
    to compare ourselves against).
  org: Engineering Function
  is_key: false
  health:
    level: 2
    reasons:
    - Engineering is now at the tech benchmark for gender diversity (~16%), but our
      potential is greater and we can do better. 20% should be our floor in technical
      roles. Other types of diversity are unknown.
    - Get data wired up and visualized in periscope.
    - Clear data sharing with legal.
    - Get better data about racial and country diversity.
  urls:
  - https://docs.google.com/presentation/d/1Rj6brdP2hKU1zgj0ll1poywBw0aR9jIAdT4GsLZfIIU/edit#slide=id.g4bb9f44e0b_6_160
  - https://docs.google.com/spreadsheets/d/1Jef9oopd8AeZRYM2P9B8ye2TxfdzxI0FY6ixhlyzW_c/edit#gid=1035217495
- name: Engineering Handbook Update Frequency
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**`
    over time.
  target: Unknown until we reach maturity level 2
  org: Engineering Function
  is_key: true
  health:
    level: 3
    reasons:
    - Above target and trending up
  sisense_data:
    chart: 8072801
    dashboard: 620621
    shared_dashboard: 67555d1d-ab13-4f02-b189-92c83bf4603b
    embed: v2
- name: Team Member Retention
  base_path: "/handbook/engineering/performance-indicators/"
  definition: People are a priority and attrition comes at a great human cost to the
    individual and team. Additionally, recruiting (backfilling attrition) is a ludicrously
    expensive process, so we prefer to keep the people we have :)
  target: 90%
  org: Engineering Function
  is_key: false
  health:
    level: 3
    reasons:
    - I seem to recall our attrition is now below 10% which is great compared to the
      tech benchmark of 22% and the remote benchmark for 16%, but the fact that I
      can’t just look at a simple graph makes me nervous...
    - Get into a system (BambooHR?) and visualized in periscope
    - Break out voluntary and involuntary
  urls:
  - https://docs.google.com/spreadsheets/d/1Jef9oopd8AeZRYM2P9B8ye2TxfdzxI0FY6ixhlyzW_c/edit#gid=1405344200
- name: Engineering Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/index.html#engineering-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Engineering function
    every month.
  target: 10%
  org: Engineering Function
  is_key: false
  health:
    level: 0
    reasons:
    - We currently track bonuses at the department level, but not at the Engineering-function
      level.
  urls:
  - 
- name: R&D Overall MR Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: This is the total number of MRs that go into the product on a monthly
    basis (regardless if the author is a GitLab team member or an open source contributor)
    divided by the total headcount of the GitLab Inc. R&D (the Product Management and
    Engineering divisions, but not the Support department). This metric measures how
    efficiently we are able to convert our investor's capital and the energy of our
    community to develop product and deliver value to users. The largest negative driver
    of this metric have been periods of intense recruiting and explosive headcount
    growth (100% in 2018, 130% in 2019). And the largest positive drivers are our
    iteration value (work breakdown) and open source contributions.
  target: Greater than 6 MRs per month
  org: Engineering Function
  is_key: true
  health:
    level: 2
    reasons:
    - Need to assess since this is a new KPI
  sisense_data:
    chart: 9287585
    dashboard: 710733
    embed: v2
- name: Engineering Division Narrow MR Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: Engineering <a href="/handbook/engineering/#merge-request-rate">MR Rate</a>
    is a key indicator showing how productive our team members are based on the average
    MR merged per team member. We currently count all members of the Engineering Division
    (Director, EMs, ICs) in the denominator because this is a team effort and we want
    to be cognizant of managerial overhead.
  target: Greater than 6 MRs per Month
  org: Engineering Function
  is_key: false
  health:
    level: 1
    reasons:
    - We have found some bugs in the data, so we need to confirm it's accurate before
      taking any action
    - We need to pick a reasonable target
  sisense_data:
    chart: 8934444
    dashboard: 686926
    shared_dashboard: b7c0d805-08d4-450a-baca-7d9496f5d142
    embed: v2
- name: Engineering Division New Hire Average Location Factor
  base_path: "/handbook/engineering/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
   happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.58
  org: Engineering Function
  is_key: true
  health:
    level: 2
    reasons:
    - We're about 0.4 over our target
    - We need to zoom into some Engineering departments that are over and tail them
      in
  sisense_data:
    chart: 9400836
    dashboard: 719537
    embed: v2
