---
layout: markdown_page
title: "GitLab Sweepstakes"
description: "View a full listing of current and previous GitLab Sweepstakes giveways. Find more information here!"
canonical_path: "/community/sweepstakes/"
---
## Current and previous giveaways

##### Current
- [Commit Pic Selfie Contest at GitLab Commit Virtual 2020](/community/sweepstakes/commit-pic-photo-booth-contest/)
- [2020 Google Anthos GitOps Webcast Sweepstakes](/community/sweepstakes/2020-Google-Anthos-GitOps-Webcast2) 
- [2020 GitLab KubeCon Quiz](/community/sweepstakes/2020-gitlab-kubecon-quiz)

##### Past
- [2020 Global Dev Sec Ops Survey and Sweepstakes](/community/sweepstakes/2020-developer-survey/) 
- [Contribute](/community/sweepstakes/contribute)
- [Just Commit](/community/sweepstakes/just-commit)
- [2019 Global Developer Survey Sweepstakes](/community/sweepstakes/2019-developer-survey.index.html)
- [2018 Global Developer Survey Sweepstakes](/community/sweepstakes/2018-developer-survey/)
- [GitLab UX Research Amazon Gift Card Giveaway](/community/sweepstakes/40-dollar-amazon-gift-card/)
- [Content Hack Day April 2018](/community/sweepstakes/content-hack-day/)
- [GitLab Original Shirt Giveaway](/community/sweepstakes/gitlab-original-tee/)
