---
layout: markdown_page
title: "GitLab vs GitHub for Version Control and Collaboration"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab Isssue Management Capabilities Missing in GitHub


| GitLab Capability                                                                         | Features                                                                                                                          |
|-------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| Plan, measure and <br>track complete development lifecycle <br>from plan to deployment    | Issue Weights, Milestones, Issue Due Dates, Time Tracking, Burndown Charts, Project Issue Board                                   |
| Reference and <br>maintain issues across multiple projects<br>, automate issue management | Move Issues to other projects, Related Issues, Mark issues as duplicate, Configurable Issue Closing Pattern, Quick Actions, Todos |


## GitLab Source Code Management Capabilities Missing in GitHub

| GitLab Capability                                           | Features                                                                                                                                                                                                                                                                |
|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Easily create, import and export projects                   | Import feature set (GitHub, Bitbucket, Google Code, FogBugz, Gitea and from any git URL)<br>Create projects with Git Push<br>Create new branches from issues<br>Export projects to external systems<br>Instance File Templates (.gitignore, .dockerfile, .license etc.) |
| Code Review capability tailored for large development teams | Multiple approvers in code review, <br>Approval rules for code review, <br>Inline commenting and discussion resolution                                                                                                                                                  |
| Model and manage large, complex projects and teams          | Subgroups within Groups to manage large projects, Group File Templates (templates at the group level to drive standardization), Group drop-down in navigation to easily find groups,                                                                                    |
| Increased development velocity                              | Web IDE to resolve feedback, fix failing tests, review changes, Web Terminal for Web IDE                                                                                                                                                                                |
| Broader support for repo types                              | Mono Repos, Conan (C++), Go, Composter (PHP),  PyPI (Python), RPM (Linux), Debian (Linux)                                                                                                                                                                               |
