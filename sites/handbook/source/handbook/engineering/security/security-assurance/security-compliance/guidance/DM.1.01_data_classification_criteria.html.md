---
layout: handbook-page-toc
title: "DM.1.01 - Data Classification Criteria"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# DM.1.01 - Data Classification Criteria

## Control Statement

A data classification policy is in place to establish a framework for classifying, securing and handling data. The policy is available in the Employee Handbook to all internal and external system users and reviewed and approved by management annually.

## Context

This control demonstrates that a data classification policy is currently in place, available, and reviewed annually. It provides classification coverage and handling requirements for various data levels.

## Scope

The GitLab Data Classification Policy applies to all data handled, managed, stored, or transmitted by GitLab and GitLab team members, including that which is submitted to GitLab Support as part of a support request.

## Ownership

* Control Owner: `IT Ops`
* Process owner(s): 
    * IT Ops: `100%`

## Guidance

The policy outlines proper handling and storage requirements for various data classification levels.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in this [control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1693).

Examples of evidence an auditor might request to satisfy this control:

* Screenshot or link to the data classification policy
* Screenshot of Version history and issue noting approval by management 

### Policy Reference

[Data Classification Policy](/handbook/engineering/security/data-classification-policy.html)

## Framework Mapping

* SOC2
  * CC3.2
  * CC6.5
